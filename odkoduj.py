# # -*- coding: utf-8 -*-
import hashlib

from Crypto.Cipher import AES


def decrypt_file(path, imie_nazwisko):
    with open(path, 'r') as file:
        with open('decoded_file.txt', 'w+') as decoded_f:
            file_txt = file.read()
            print len(file_txt)
            try:
                sha_val = hashlib.md5(str(imie_nazwisko)).hexdigest()
                obj2 = AES.new(sha_val[0:16], AES.MODE_CBC, sha_val[16:32])
                decoded = obj2.decrypt(str(file_txt))
                decoded_f.write(decoded)
            except Exception as e:
                print e
                raise Exception()


if __name__ == '__main__':
    decrypt_file('sciezka_do_pliku', 'imie_nazwisko')