from OpenSSL import crypto

key_file = open('private_key.pem', 'r')
key = crypto.load_privatekey(crypto.FILETYPE_PEM, key_file.read())

ca_cert_file = open('moj_certyfikat_podpisany_przez_kogos.pem', 'r')
ca_cert = crypto.load_certificate(crypto.FILETYPE_PEM, ca_cert_file.read())

csr_file = open('csr_podpisywanego.pem', 'r')
csr = crypto.load_certificate_request(crypto.FILETYPE_PEM, csr_file.read())

cert = crypto.X509()
cert.set_subject(csr.get_subject())
cert.set_pubkey(csr.get_pubkey())
cert.set_issuer(ca_cert.get_subject())
cert.set_serial_number(5)
cert.gmtime_adj_notBefore(0)
cert.gmtime_adj_notAfter(60 * 60 * 24 * 365)
cert.sign(key, 'sha256')
print crypto.dump_certificate(crypto.FILETYPE_PEM, cert)