# README #

### Podpisywanie certyfikatów ###

1 Nalgenerować własny klucz prywatny

openssl genrsa -out private_key.pem 2048

2 Należy wygenerować prośbę o certyfikat 

openssl req -new -key private_key.pem -out csr.pem

3 Aby stworzyć certyfikat podpisany przez siebie (własnym kluczem prywatnym) korzystamy z polecenia:

openssl x509 -req -days 23 -in csr.pem -signkey private_key.pem -out cert.pem

4 Aby podpisać czyjś request(plik.csr) można skorzystać z polecenia:

openssl x509 -req -in csr.pem  -CA cert1.pem -CAkey private_key.pem -CAcreateserial -out cert2.pem -days 321

Można również skorzystać z koduy znajdującego się w repozytporium
